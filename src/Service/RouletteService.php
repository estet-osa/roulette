<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RouletteService
{
    private $session;

    /**
     * RouletteService constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param int $user
     */
    public function setUserSession(int $user)
    {
        $this->session->set('user', $user);
    }

    /**
     * @return mixed
     */
    public function getUserSession()
    {
        return $this->session->get('user');
    }
}
