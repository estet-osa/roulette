<?php

namespace App\Controller;

use App\Entity\{Logs, Roulette};
use App\Form\Type\RouletteType;
use App\Repository\{LogsRepository, RouletteRepository};
use App\Service\RouletteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\{Request, Response};
use FOS\RestBundle\Controller\Annotations as Rest;

class RouletteController extends AbstractController
{
    /**
     * Render form and show current INFO about roulette status.
     *
     * @Rest\Get("/roulette", name="roulette_get")
     *
     * @param RouletteService $rouletteService
     *
     * @return Response
     */
    public function rouletteGet(RouletteService $rouletteService): Response
    {
        /** @var RouletteRepository $rouletteRepository */
        $rouletteRepository = $this->getDoctrine()
            ->getRepository(Roulette::class);

        return $this->render('roulette/spin.html.twig', [
            'data' => [
                'user'    => $rouletteService->getUserSession(),
                'roulette'=> $rouletteRepository
                    ->findOneBy([], ['id' => 'desc'])
            ],
            'form' => $this->createForm(RouletteType::class)->createView()
        ]);
    }

    /**
     * Spin roulette.
     *
     * @Rest\Post("/roulette", name="roulette_post")
     *
     * @param Request $request
     * @param RouletteService $rouletteService
     *
     * @return Response
     */
    public function roulettePost(Request $request, RouletteService $rouletteService): Response
    {
        /** @var int|null $user */
        $user = $rouletteService->getUserSession();

        if (null === $user){
            return $this->redirectToRoute('index_get');
        }
        /** @var FormInterface $form */
        $form = $this->createForm(RouletteType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var RouletteRepository $rouletteRepository */
            $rouletteRepository = $this->getDoctrine()
                ->getRepository(Roulette::class);

            /** @var LogsRepository $logsRepository */
            $logsRepository = $this->getDoctrine()
                ->getRepository(Logs::class);

            /** @var Roulette $roulette */
            $roulette = $rouletteRepository->spin();

            if (Roulette::STATE_OVER !== $roulette->getState()) {
                $logsRepository->save($roulette, $user);
            }
            return $this->redirectToRoute('roulette_get');
        }
        return $this->render('roulette/spin.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
