<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\UserType;
use App\Service\RouletteService;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\{Request, Response};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;

class IndexController extends AbstractController
{
    /**
     * @Rest\Get("/", name="index_get")
     *
     * @return Response
     */
    public function indexGet(): Response
    {
        return $this->render('roulette/index.html.twig', [
            'form' => $this
                ->createForm(UserType::class)
                ->createView()
        ]);
    }

    /**
     * @Rest\Post("/", name="index_post")
     *
     * @param Request $request
     * @param RouletteService $rouletteService
     *
     * @return Response
     */
    public function indexPost(Request $request, RouletteService $rouletteService): Response
    {
        /** @var FormInterface $form */
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var int $user */
            $user = $form->get('id')->getData();

            if (null === $user
                || $user === User::NO_CHANGE_USER
            ) {
                return $this->redirectToRoute('index_get');
            } else {
                $rouletteService->setUserSession($user);
                return $this->redirectToRoute('roulette_post');
            }
        }
        return $this->render('roulette/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
