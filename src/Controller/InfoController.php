<?php

namespace App\Controller;

use App\Entity\Logs;
use App\Repository\LogsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/info", name="info", methods="GET")
 */
class InfoController extends AbstractController
{
    /**
     * @Rest\Get("/members", name="get_info_members")
     *
     * @return JsonResponse
     */
    public function getMembers(): JsonResponse
    {
        /** @var LogsRepository $repo */
        $repo = $this->getDoctrine()
            ->getRepository(Logs::class);

        return $this->json($repo->findMembers());
    }

    /**
     * @Rest\Get("/actives", name="get_info_actives")
     *
     * @return JsonResponse
     */
    public function getActives(): JsonResponse
    {
        /** @var LogsRepository $repo */
        $repo = $this->getDoctrine()
            ->getRepository(Logs::class);

        return $this->json($repo->findActives());
    }
}
