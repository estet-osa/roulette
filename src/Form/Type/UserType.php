<?php

namespace App\Form\Type;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', ChoiceType::class, [
                'label'      => false,
                'required'   => true,
                'choices'    => User::getUsers(),
                'placeholder'=> '-- Не выбрано --',
            ])
            ->add('send', SubmitType::class, [
                'label' => 'Начать игру',
                'attr'  => [
                    'class' => 'btn'
                ]
            ])
        ;
    }
}
