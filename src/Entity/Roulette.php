<?php

namespace App\Entity;

use App\Repository\RouletteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RouletteRepository::class)
 */
class Roulette
{
    const STATE_START = 1,
          STATE_OVER  = 11;

    /** @var array $cells */
    public static $cells = [
        1 => 20,
        2 => 100,
        3 => 45,
        4 => 70,
        5 => 15,
        6 => 140,
        7 => 20,
        8 => 20,
        9 => 140,
        10=> 45,
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $state;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->state;
    }

    /**
     * @param int $state
     *
     * @return Roulette
     */
    public function setState(int $state = self::STATE_START): self
    {
        $this->state = $state;

        return $this;
    }
}
