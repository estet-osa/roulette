<?php

namespace App\Entity;

use App\Repository\LogsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LogsRepository::class)
 */
class Logs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $user;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $cell;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Roulette")
     * @ORM\JoinColumn(name="roulette_id", referencedColumnName="id")
     */
    private $roulette;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getUser(): ?int
    {
        return $this->user;
    }

    /**
     * @param int $user
     *
     * @return Logs
     */
    public function setUser(int $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCell(): ?int
    {
        return $this->cell;
    }

    /**
     * @param int $cell
     *
     * @return Logs
     */
    public function setCell(int $cell): self
    {
        $this->cell = $cell;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoulette(): ?int
    {
        return $this->roulette;
    }

    /**
     * @param Roulette $roulette
     *
     * @return Logs
     */
    public function setRoulette(Roulette $roulette): self
    {
        $this->roulette = $roulette;

        return $this;
    }
}
