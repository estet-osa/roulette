<?php

namespace App\Entity;

class User
{
    const NO_CHANGE_USER = 0;

    /**
     * @return array
     */
    public static function getUsers(): array
    {
        $users = range(0, 9);
        unset($users[self::NO_CHANGE_USER]);

        return $users;
    }
}
