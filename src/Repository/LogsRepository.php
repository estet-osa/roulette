<?php

namespace App\Repository;

use App\Entity\Logs;
use App\Entity\Roulette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Logs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Logs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Logs[]    findAll()
 * @method Logs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogsRepository extends ServiceEntityRepository
{
    /**
     * LogsRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Logs::class);
    }

    /**
     * @param Roulette $roulette
     * @param int $user
     */
    public function save(Roulette $roulette, int $user): void
    {
        /** @var array $logs */
        $logs = $this->findBy(['roulette' => $roulette]);

        /** @var array $cells - get cells from array collection */
        $cells = array_map(function(Logs $log) {
            return $log->getCell();
        }, $logs);
        /** Sorting cells array */
        $this->bubbleSort(Roulette::$cells);

        /** @var array $diffArray - calculate array difference */
        $diffArray = array_diff_key(Roulette::$cells, array_flip($cells));

        if (null !== key($diffArray))
            $this->new($roulette, $user, key($diffArray));
    }

    /**
     * @param $cells
     *
     * @return bool
     */
    function bubbleSort(&$cells): bool
    {
        return asort($cells);
    }

    /**
     * @param Roulette $roulette
     * @param int $user
     * @param int $cell
     *
     * @return Logs
     */
    public function new(Roulette $roulette, int $user, int $cell): Logs
    {
        /** @var Logs $logs */
        $logs = (new Logs())
            ->setRoulette($roulette)
            ->setUser($user)
            ->setCell($cell);

        $this->_em->persist($logs);
        $this->_em->flush();

        return $logs;
    }

    /**
     * @return array|null
     */
    public function findMembers(): ?array
    {
        /** @var ResultSetMapping $rsm */
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('roulette_id', 'id');
        $rsm->addScalarResult('users', 'users');

        return $this->getEntityManager()
            ->createNativeQuery('
                SELECT
                    roulette_id,
                    count(DISTINCT `user`) AS users
                  FROM logs
                GROUP BY roulette_id
            ', $rsm)
            ->getResult();
    }

    /**
     * @return array|null
     */
    public function findActives(): ?array
    {
        /** @var ResultSetMapping $rsm */
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('user', 'user');
        $rsm->addScalarResult('rounds', 'rounds');
        $rsm->addScalarResult('average', 'average');

        return $this->getEntityManager()
            ->createNativeQuery('
                SELECT
                    `user`,
                    count(DISTINCT roulette_id) AS rounds,
                    round(avg(roulette_id)) AS average
                  FROM (SELECT
                        `user`,
                        count(roulette_id) AS roulette_id
                          FROM logs
                        GROUP BY user, roulette_id) item
                GROUP BY user
            ', $rsm)
            ->getResult();
    }
}
