<?php

namespace App\Repository;

use App\Entity\Roulette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Roulette|null find($id, $lockMode = null, $lockVersion = null)
 * @method Roulette|null findOneBy(array $criteria, array $orderBy = null)
 * @method Roulette[]    findAll()
 * @method Roulette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RouletteRepository extends ServiceEntityRepository
{
    /**
     * RouletteRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Roulette::class);
    }

    /**
     * @return Roulette|null
     */
    public function spin(): ?Roulette
    {
        /** @var Roulette $roulette - get last roulette */
        $roulette = $this->findOneBy([], ['id' => 'desc']);

        if (null === $roulette
            || Roulette::STATE_OVER === $roulette->getState()
        ) {
            $roulette = $this->new();
        }else{
            $roulette = $this->update($roulette);
        }
        return $roulette;
    }

    /**
     * Increment state field in Roulette.
     *
     * @param Roulette $roulette
     *
     * @return Roulette
     */
    public function update(Roulette $roulette): Roulette
    {
        $roulette->setState(
            $roulette->getState() + Roulette::STATE_START
        );
        $this->_em->persist($roulette);
        $this->_em->flush();

        return $roulette;
    }

    /**
     * Create new round.
     *
     * @return Roulette
     */
    public function new(): Roulette
    {
        /** @var Roulette $roulette */
        $roulette = (new Roulette())->setState();

        $this->_em->persist($roulette);
        $this->_em->flush();

        return $roulette;
    }
}
